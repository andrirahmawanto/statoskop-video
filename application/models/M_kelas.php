<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_kelas extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
  function getKelas1(){
   $this->db->select('*');
   $this->db->from('tb_siswa');
   $query = $this->db->get();
   return $query->result();
 } 
 function getKelas2(){
  $this->db->select('*');
  $this->db->from('tb_siswa');
  $query = $this->db->get();
  return $query->result();
} 
function getKelas3(){
  $this->db->select('*');
  $this->db->from('tb_siswa');
  $query = $this->db->get();
  return $query->result();
} 
public function get_max() {
 $query = $this->db->query("SELECT IFNULL(MAX(id_pengguna)+1,1) AS max_id FROM tb_pengguna ");        
 $row     = $query->row_array();
 $max_id  = $row['max_id'];
 return $max_id;
}
function insert_guru($data1){
  $this->db->insert('tb_guru',$data1);
}
function delete_guru($id) {
  $this->db->where('id_pengguna', $id);
  $this->db->delete('tb_guru');
  if ($this->db->affected_rows() == 1) {
    return TRUE;
  }
  return FALSE;
}
function update_guru($data,$id_pengguna){
  $this->db->where('id_pengguna',$id_pengguna);
  $this->db->update('tb_guru',$data);
} 
}
?>
