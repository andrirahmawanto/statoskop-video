<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_user extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
   function getUser(){
   		$this->db->select('*');
   		$this->db->from('tb_user');
   		$query = $this->db->get();
   		return $query->result();
   } 
 function insert_guru($data1){
  $this->db->insert('tb_guru',$data1);
 }
     function delete_guru($id) {
        $this->db->where('id_pengguna', $id);
        $this->db->delete('tb_guru');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  	function update_guru($data,$id_pengguna){
		$this->db->where('id_pengguna',$id_pengguna);
		$this->db->update('tb_guru',$data);
	}
}
?>
