<?php if(!defined('BASEPATH')) exit('Hacking Attempt : Keluar dari sistem..!!');

class M_tatib extends CI_Model
{
  public function __construct()
  {
    parent::__construct();
  } 
   function getTatib(){
   		$this->db->select('*');
   		$this->db->from('tb_tatib');
   		$query = $this->db->get();
   		return $query->result();
   } 
   function getSiswa(){
   		$this->db->select('*');
   		$this->db->from('v_pengguna_siswa');
   		$query = $this->db->get();
   		return $query->result();
   } 
   function getAlumni(){
   		$this->db->select('*');
   		$this->db->from('v_pengguna_alumni');
   		$query = $this->db->get();
   		return $query->result();
   } 
   public function get_max() {
   		$query = $this->db->query("SELECT IFNULL(MAX(id_pengguna)+1,1) AS max_id FROM tb_pengguna ");        
   		$row     = $query->row_array();
   		$max_id  = $row['max_id'];
   return $max_id;
 }
 function insert_guru($data1){
  $this->db->insert('tb_guru',$data1);
 }
  function insert_siswa($data1){
  $this->db->insert('tb_siswa',$data1);
 }
  function insert_alumni($data1){
  $this->db->insert('tb_alumni',$data1);
 }  
 function insert_pengguna($data2){
  $this->db->insert('tb_pengguna',$data2);
 }
     function delete_guru($id) {
        $this->db->where('id_pengguna', $id);
        $this->db->delete('tb_guru');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  	function delete_pengguna($id) {
        $this->db->where('id_pengguna', $id);
        $this->db->delete('tb_pengguna');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  	function update_guru($data,$id_pengguna){
		$this->db->where('id_pengguna',$id_pengguna);
		$this->db->update('tb_guru',$data);
	}
  function update_alumni($data,$id_pengguna){
		$this->db->where('id_pengguna',$id_pengguna);
		$this->db->update('tb_alumni',$data);
	}
  function update_siswa($data,$id_pengguna){
		$this->db->where('id_pengguna',$id_pengguna);
		$this->db->update('tb_siswa',$data);
	}
  function delete_alumni($id) {
        $this->db->where('id_pengguna', $id);
        $this->db->delete('tb_alumni');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  function delete_siswa($id) {
        $this->db->where('id_pengguna', $id);
        $this->db->delete('tb_siswa');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  function getKelas(){
   $this->db->select('*');
   $this->db->from('tb_kelas');
   $query = $this->db->get();
   return $query->result();
 } 
  function getJurusan(){
   $this->db->select('*');
   $this->db->from('tb_jurusan');
   $query = $this->db->get();
   return $query->result();
 } 
}
?>
