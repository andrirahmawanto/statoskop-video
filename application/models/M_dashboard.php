<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_dashboard extends CI_Model {        
  function __construct() {
    parent::__construct();
  }
  function getTotalNilai1($id_pengguna){   
       $this->db->select('*');
       $this->db->from('v_nilai_total_ipa');
       $this->db->where('id_pengguna',$id_pengguna);
       $query = $this->db->get();
       return $query->result();
    }
    function getTotalNilai2($id_pengguna){   
       $this->db->select('*');
       $this->db->from('v_nilai_total_ips');
       $this->db->where('id_pengguna',$id_pengguna);
       $query = $this->db->get();
       return $query->result();
    }
      function getTotalNilai3(){   
       $this->db->select('*');
       $this->db->from('v_nilai_total_ipa');
       $query = $this->db->get();
       return $query->result();
    }
    function getTotalNilai4(){   
       $this->db->select('*');
       $this->db->from('v_nilai_total_ips');
       $query = $this->db->get();
       return $query->result();
    }
    function getPilProdi(){   
       $this->db->select('*');
       $this->db->from('v_pilihan_prodi');
       $query = $this->db->get();
       return $query->result();
    }
    function getPilProdiById($id_pengguna){   
       $this->db->select('*');
       $this->db->from('v_pilihan_prodi');
       $this->db->where('id_pengguna',$id_pengguna);
       $query = $this->db->get();
       return $query->result();
    }
    function getTotalSiswa(){   
       $this->db->select('*');
       $this->db->from('v_jumlah_siswa');
       $query = $this->db->get();
       return $query->result();
    }
    function getRank1($id_pengguna){   
       $this->db->select('*');
       $this->db->from('v_rank_ipa');
       $this->db->where('id_pengguna',$id_pengguna);
       $query = $this->db->get();
       return $query->result();
    }
  	function getRank2($id_pengguna){   
       $this->db->select('*');
       $this->db->from('v_rank_ips');
       $this->db->where('id_pengguna',$id_pengguna);
       $query = $this->db->get();
       return $query->result();
    }
}