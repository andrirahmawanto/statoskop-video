<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_kelola extends CI_Model {        
  function __construct() {
    parent::__construct();
  }
    function getGuru(){   
       $this->db->select('*');
       $this->db->from('tb_guru');
       $query = $this->db->get();
       return $query->result();
    }
    function getSiswa(){   
       $this->db->select('*');
       $this->db->from('tb_siswa');
       $query = $this->db->get();
       return $query->result();
    }
  	function getKelas(){   
       $this->db->select('*');
       $this->db->from('tb_kelas');
       $query = $this->db->get();
       return $query->result();
    }
  function insert_guru($data){
  		$this->db->insert('tb_jurusan',$data);
 	}  
    function insert_siswa($data){
      $this->db->insert('tb_mapel',$data);
  }
  function insert_kelas($data){
  		$this->db->insert('tb_kelas',$data);
 	}
  function update_ptn($data,$id_perguruan_tinggi){
		$this->db->where('id_perguruan_tinggi',$id_perguruan_tinggi);
		$this->db->update('tb_perguruan_tinggi',$data);
	}
  function delete_ptn($id) {
        $this->db->where('id_perguruan_tinggi', $id);
        $this->db->delete('tb_perguruan_tinggi');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  function update_prodi($data,$id_prodi){
		$this->db->where('id_prodi',$id_prodi);
		$this->db->update('tb_prodi',$data);
	}
  function delete_prodi($id) {
        $this->db->where('id_prodi', $id);
        $this->db->delete('tb_prodi');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  function update_kelas($data,$id_kelas){
		$this->db->where('id_kelas',$id_kelas);
		$this->db->update('tb_kelas',$data);
	}
  function delete_kelas($id) {
        $this->db->where('id_kelas', $id);
        $this->db->delete('tb_kelas');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  function update_jurusan($data,$id_jurusan){
		$this->db->where('id_jurusan',$id_jurusan);
		$this->db->update('tb_jurusan',$data);
	}
  function delete_jurusan($id) {
        $this->db->where('id_jurusan', $id);
        $this->db->delete('tb_jurusan');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
  function update_mapel($data,$id_mapel){
		$this->db->where('id_mapel',$id_mapel);
		$this->db->update('tb_mapel',$data);
	}
  function delete_mapel($id) {
        $this->db->where('id_mapel', $id);
        $this->db->delete('tb_mapel');
    if ($this->db->affected_rows() == 1) {
        return TRUE;
    }
        return FALSE;
    }
}