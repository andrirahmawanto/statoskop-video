<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model('M_dashboard');
        $this->load->helper('form','url','number');
        $this->load->library('pdf');
        $this->load->library('form_validation');
        if(empty($this->session->userdata('username'))) {
            $this->session->set_flashdata('isLogin',false);
            redirect('Login');
        }
    }  
    public function index(){
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        if($this->session->userdata('level') == 1){            
            $this->load->view('v_dashboard_admin');
        }else{            
            $this->load->view('v_dashboard_kesiswaan');
        }
        $this->load->view('layout/footer');
    }
    // public function cetak(){
    //     $id_pengguna = $this->session->userdata('id_pengguna');
    //     $x['data']   = $this->M_dashboard->getTotalNilai1($id_pengguna);
    //     $x['data1']  = $this->M_dashboard->getTotalNilai2($id_pengguna);
    //     $x['data5']  = $this->M_dashboard->getPilProdiById($id_pengguna);
    //     $x['data6']  = $this->M_dashboard->getRank1($id_pengguna);
    //     $x['data7']  = $this->M_dashboard->getRank2($id_pengguna);
    //     $this->load->view('v_cetak',$x);
    // }
}