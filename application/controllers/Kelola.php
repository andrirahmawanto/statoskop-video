<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kelola extends CI_Controller {
	public function __construct() {
        parent::__construct();
        $this->load->model('M_kelola');
        $this->load->helper('form','url','number');
        $this->load->library('form_validation');
        if(empty($this->session->userdata('username'))) {
            $this->session->set_flashdata('isLogin',false);
            redirect('Login');
        }
    }    
  	public function guru(){
        $x['data']   	= $this->M_kelola->getGuru(); 
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('v_kelola_guru', $x);
        $this->load->view('layout/footer');
    }
  	public function siswa(){
        $x['data']   	= $this->M_kelola->getSiswa(); 
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('v_kelola_siswa', $x);
        $this->load->view('layout/footer');
    }
  	public function kelas(){
        $x['data']   	= $this->M_kelola->getKelas(); 
        $this->load->view('layout/header');
        $this->load->view('layout/sidebar');
        $this->load->view('v_kelola_kelas', $x);
        $this->load->view('layout/footer');
    }
  function input_ptn(){
        if(isset($_POST['btnsimpan'])){
		$nama_ptn              = $this->input->post('nama_ptn');
		$data = array(
			'nama_perguruan_tinggi'             => $nama_ptn
			);
			$this->M_kelola->insert_ptn($data);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}  
  function update_ptn(){
        if(isset($_POST['btnsimpan'])){
        $id_perguruan_tinggi   = $this->input->post('id_perguruan_tinggi'); 
        $nama_ptn              = $this->input->post('nama_ptn'); 
			
		$data = array(  
			'nama_perguruan_tinggi'	 	 	    => $nama_ptn
		);
		$this->M_kelola->update_ptn($data, $id_perguruan_tinggi);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function delete_ptn(){
		if(isset($_POST['btndelete'])){
			$id = $this->input->post('id_perguruan_tinggi');
			$this->M_kelola->delete_ptn($id);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
  	function input_prodi(){
        if(isset($_POST['btnsimpan'])){
		$id_perguruan_tinggi	= $this->input->post('id_perguruan_tinggi');
		$nama_prodi             = $this->input->post('nama_prodi'); 
		$ket		            = $this->input->post('ket'); 
			$data = array(
				'id_perguruan_tinggi'		=> $id_perguruan_tinggi,
				'nama_prodi'             	=> $nama_prodi,
				'keterangan'             	=> $ket
			);
			$this->M_kelola->insert_prodi($data);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}  
  function update_prodi(){
        if(isset($_POST['btnsimpan'])){
        $id_perguruan_tinggi	= $this->input->post('id_perguruan_tinggi');
        $id_prodi				= $this->input->post('id_prodi');
		$nama_prodi             = $this->input->post('nama_prodi'); 
		$ket		            = $this->input->post('ket'); 
			
		$data = array(  
			'id_perguruan_tinggi'	 	 	    => $id_perguruan_tinggi,
			'nama_prodi'	 	 	  			=> $nama_prodi,
			'keterangan'			 	 	    => $ket
		);
		$this->M_kelola->update_prodi($data, $id_prodi);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function delete_prodi(){
		if(isset($_POST['btndelete'])){
			$id = $this->input->post('id_prodi');
			$this->M_kelola->delete_prodi($id);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
  function input_kelas(){
        if(isset($_POST['btnsimpan'])){
		$nama_kelas	            = $this->input->post('nama_kelas');
		$data = array(
			'nama_kelas'             => $nama_kelas
			);
			$this->M_kelola->insert_kelas($data);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}  
  function update_kelas(){
        if(isset($_POST['btnsimpan'])){
        $id_kelas			   = $this->input->post('id_kelas'); 
        $nama_kelas            = $this->input->post('nama_kelas'); 
			
		$data = array(  
			'nama_kelas'	 	 	    => $nama_kelas
		);
		$this->M_kelola->update_kelas($data, $id_kelas);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function delete_kelas(){
		if(isset($_POST['btndelete'])){
			$id = $this->input->post('id_kelas');
			$this->M_kelola->delete_kelas($id);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
  function input_jurusan(){
        if(isset($_POST['btnsimpan'])){
		$nama_jurusan	            = $this->input->post('nama_jurusan');
		$data = array(
			'nama_jurusan'             => $nama_jurusan
			);
			$this->M_kelola->insert_jurusan($data);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}  
  function update_jurusan(){
        if(isset($_POST['btnsimpan'])){
        $id_jurusan			   = $this->input->post('id_jurusan'); 
        $nama_jurusan          = $this->input->post('nama_jurusan'); 
			
		$data = array(  
			'nama_jurusan'	 	 	    => $nama_jurusan
		);
		$this->M_kelola->update_jurusan($data, $id_jurusan);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function delete_jurusan(){
		if(isset($_POST['btndelete'])){
			$id = $this->input->post('id_jurusan');
			$this->M_kelola->delete_jurusan($id);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
  function input_mapel(){
        if(isset($_POST['btnsimpan'])){
		$nama_mapel	            = $this->input->post('nama_mapel');
		$data = array(
			'nama_mapel'             => $nama_mapel
			);
			$this->M_kelola->insert_mapel($data);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}  
  function update_mapel(){
        if(isset($_POST['btnsimpan'])){
        $id_mapel			   = $this->input->post('id_mapel'); 
        $nama_mapel          = $this->input->post('nama_mapel'); 
			
		$data = array(  
			'nama_mapel'	 	 	    => $nama_mapel
		);
		$this->M_kelola->update_mapel($data, $id_mapel);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function delete_mapel(){
		if(isset($_POST['btndelete'])){
			$id = $this->input->post('id_mapel');
			$this->M_kelola->delete_mapel($id);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}