<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {
	public function __construct() {
		parent::__construct();
		$this->load->model('M_user');
		$this->load->helper('form','url','number');
		$this->load->library('form_validation');
		if(empty($this->session->userdata('username'))) {
            $this->session->set_flashdata('isLogin',false);
            redirect('Login');
        }
	}  
	function index()
	{
		$x['data'] = $this->M_user->getUser();
		$this->load->view('layout/header');
		$this->load->view('layout/sidebar');
		$this->load->view('v_user', $x);
		$this->load->view('layout/footer');
	}
	function input_guru(){
        if(isset($_POST['btnsimpan'])){
        $id_pengguna      		= $this->M_user->get_max();     
		$password		        = md5($this->input->post('password'));     
		$nama_guru              = $this->input->post('nama_guru'); 
		$nik             		= $this->input->post('nik');
        $email          		= $this->input->post('email');
		$level		            = 2;			
		$data1 = array(
				'id_pengguna'           => $id_pengguna,
				'nama_guru'             => $nama_guru,
				'nik'                   => $nik,
				'email'              	=> $email
			);
			$this->M_user->insert_guru($data1);
			$data2 = array(
			    'id_pengguna'       => $id_pengguna,
				'password'          => $password,
				'level'  		    => $level
			);
			$this->M_user->insert_pengguna($data2);
		redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}  
  	function update_guru(){
        if(isset($_POST['btnsimpan'])){
		$id_pengguna			= $this->input->post('id_pengguna');
        $nama_guru              = $this->input->post('nama_guru'); 
		$nik             		= $this->input->post('nik');
        $email          		= $this->input->post('email');
			
		$data = array(  
			'nama_guru'	 	 	    => $nama_guru,
			'nik'					=> $nik,
			'email'					=> $email
		);
		$this->M_user->update_guru($data, $id_pengguna);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
	function delete_guru(){
		if(isset($_POST['btndelete'])){
			$id = $this->input->post('id_pengguna');
			$this->M_user->delete_guru($id);
          	$this->M_user->delete_pengguna($id);
			redirect($_SERVER['HTTP_REFERER']);
		}else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}
}
