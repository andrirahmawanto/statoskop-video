            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DASHBOARD</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                            Dashboard
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
            <div id="bg-default" class="panel-collapse collapse in">
              <div class="portlet-body">
                <div class = "row">
                  <!-- end row -->
                  <div class="col-lg-4 col-md-6" align="center">
                    <div class="card-box widget-box-two widget-two-primary">
                      <div class="wigdet-two-content">
                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total Siswa</p>
                        <h2><span>2</span></h2>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="col-lg-4 col-md-6" align="center">
                <div class="card-box widget-box-two widget-two-primary">
                  <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total IPA Mengisi</p>
                    <h2><span>4</span></h2>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="col-lg-4 col-md-6" align="center">
            <div class="card-box widget-box-two widget-two-primary">
              <div class="wigdet-two-content">
                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total IPS Mengisi</p>
                <h2><span>20</span></h2>
            </div>
        </div>
    </div>
    <!-- end row -->
</div>
</div>
</div>
</div>

