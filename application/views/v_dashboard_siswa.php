            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DASHBOARD</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                            Dashboard
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
              <div class="portlet">
                <div class="portlet-heading bg-info">
                  <h3 class="portlet-title">
                    Data PDSS
                </h3>
                <div class="portlet-widgets">
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i class="ion-minus-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="bg-default" class="panel-collapse collapse in">
              <div class="portlet-body">
                <div class = "row">
                  <?php foreach ($data5 as $rowdata5){ ?>
                  <div class="col-lg-6 col-md-6" align="center">
                    <div class="card-box widget-box-two widget-two-primary">
                      <div class="wigdet-two-content">
                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Rekomendasi <br> Perguruan Tinggi</p>
                        <h2><span></span> <?php echo $rowdata5->nama_rekomendasi_perguruan_tinggi ?> <small></small></h2>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-md-6" align="center">
                <div class="card-box widget-box-two widget-two-primary">
                  <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Rekomendasi <br> Jurusan</p>
                    <h2><span></span> <?php echo $rowdata5->nama_rekomendasi_prodi ?> <small></small></h2>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- end row -->
    </div> <!-- content -->
    <div class = "row">
        <?php
      		if($this->session->userdata('id_jurusan') == 1){
      		foreach ($data as $rowdata){ ?>
            <div class="col-lg-2 col-md-6" align="center">
              <div class="card-box widget-box-two widget-two-success">
                <!--<i class="fa fa-signing widget-two-icon"></i>-->
                <div class="wigdet-two-content">
                  <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">TOTAL NILAI <br> SEMESTER 1</p>
                  <h2><span data-plugin="counterup"><?php echo $rowdata->tot_1; ?></span></h2>
              </div>
          </div>
      </div>
      <div class="col-lg-2 col-md-6" align="center">
          <div class="card-box widget-box-two widget-two-success">
            <!--<i class="mdi mdi-thermometer widget-two-icon"></i>-->
            <div class="wigdet-two-content">
              <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">TOTAL NILAI <br> SEMESTER 2</p>
              <h2><span data-plugin="counterup"><?php echo $rowdata->tot_2; ?></span></h2>
          </div>
      </div>
  </div>
  <div class="col-lg-4 col-md-6" align="center">
      <div class="card-box widget-box-two widget-two-success">
        <!--<i class="mdi mdi-arrow-expand-all widget-two-icon"></i>-->
        <div class="wigdet-two-content">
          <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">TOTAL NILAI <br> SEMESTER 3</p>
          <h2><span data-plugin="counterup"><?php echo $rowdata->tot_3; ?></span></h2>
      </div>
  </div>
</div>
<div class="col-lg-2 col-md-6" align="center">
  <div class="card-box widget-box-two widget-two-success">
    <!--<i class="mdi mdi-shield-outline widget-two-icon"></i>-->
    <div class="wigdet-two-content">
      <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">TOTAL NILAI <br> SEMESTER 4</p>
      <h2><span data-plugin="counterup"><?php echo $rowdata->tot_4; ?></span></h2>
  </div>
</div>
</div>
<div class="col-lg-2 col-md-6" align="center">
  <div class="card-box widget-box-two widget-two-success">
    <!--<i class="mdi mdi-shield-outline widget-two-icon"></i>-->
    <div class="wigdet-two-content">
      <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">TOTAL NILAI <br> SEMESTER 5</p>
      <h2><span data-plugin="counterup"><?php echo $rowdata->tot_5; ?></span></h2>
  </div>
</div>
</div>
<?php } } elseif($this->session->userdata('id_jurusan') == 2){
      foreach ($data1 as $rowdata1){ ?>
      <div class="col-lg-2 col-md-6" align="center">
              <div class="card-box widget-box-two widget-two-success">
                <!--<i class="fa fa-signing widget-two-icon"></i>-->
                <div class="wigdet-two-content">
                  <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">TOTAL NILAI <br> SEMESTER 1</p>
                  <h2><span data-plugin="counterup"><?php echo $rowdata1->tot_1; ?></span></h2>
              </div>
          </div>
      </div>
      <div class="col-lg-2 col-md-6" align="center">
          <div class="card-box widget-box-two widget-two-success">
            <!--<i class="mdi mdi-thermometer widget-two-icon"></i>-->
            <div class="wigdet-two-content">
              <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">TOTAL NILAI <br> SEMESTER 2</p>
              <h2><span data-plugin="counterup"><?php echo $rowdata1->tot_2; ?></span></h2>
          </div>
      </div>
  </div>
  <div class="col-lg-4 col-md-6" align="center">
      <div class="card-box widget-box-two widget-two-success">
        <!--<i class="mdi mdi-arrow-expand-all widget-two-icon"></i>-->
        <div class="wigdet-two-content">
          <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="User This Month">TOTAL NILAI <br> SEMESTER 3</p>
          <h2><span data-plugin="counterup"><?php echo $rowdata1->tot_3; ?></span></h2>
      </div>
  </div>
</div>
<div class="col-lg-2 col-md-6" align="center">
  <div class="card-box widget-box-two widget-two-success">
    <!--<i class="mdi mdi-shield-outline widget-two-icon"></i>-->
    <div class="wigdet-two-content">
      <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">TOTAL NILAI <br> SEMESTER 4</p>
      <h2><span data-plugin="counterup"><?php echo $rowdata1->tot_4; ?></span></h2>
  </div>
</div>
</div>
<div class="col-lg-2 col-md-6" align="center">
  <div class="card-box widget-box-two widget-two-success">
    <!--<i class="mdi mdi-shield-outline widget-two-icon"></i>-->
    <div class="wigdet-two-content">
      <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">TOTAL NILAI <br> SEMESTER 5</p>
      <h2><span data-plugin="counterup"><?php echo $rowdata1->tot_5; ?></span></h2>
  </div>
</div>
</div>
      <?php } } ?>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-12">
    <div class="portlet">
      <div class="portlet-heading bg-info">
        <h3 class="portlet-title">
          RANKING PDSS
      </h3>
      <div class="portlet-widgets">
          <span class="divider"></span>
          <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default2"><i class="ion-minus-round"></i></a>
      </div>
      <div class="clearfix"></div>
  </div>
  <div id="bg-default2" class="panel-collapse collapse in">
    <div class="portlet-body">
      <div class = "row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="ranking" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">Ranking</th>
                            <th style="text-align: center; width: 1%">NISN</th>
                            <th style="text-align: center; width: 15%">Nama Siswa</th>
                            <th style="text-align: center; width: 15%">Kelas</th>
                            <th style="text-align: center; width: 15%">Total Nilai</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
                      if($this->session->userdata('id_jurusan') == 1){
  					   $no = 0 ;
                       foreach ($data2 as $rowdata2) {   
                        $no++;              
                        ?> 
                        <tr>
                            <td style="text-align: center;"><?php echo $no ?></td>
                            <td style="text-align: left;"><?php echo $rowdata2->nisn ?></td>
                            <td style="text-align: left; white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata2->nama_siswa ?></td>
                            <td style="text-align: center;"><?php echo $rowdata2->nama_kelas ?></td>
                            <td style="text-align: center;"><?php echo $rowdata2->tot_all ?></td>
                        </tr>
                    	<?php } 
					  } elseif($this->session->userdata('id_jurusan') == 2){ 
                        $no = 0 ;
                        foreach ($data3 as $rowdata3) {   
                        $no++;              
                        ?> 
                        <tr>
                            <td style="text-align: center;"><?php echo $no ?></td>
                            <td style="text-align: left;"><?php echo $rowdata3->nisn ?></td>
                            <td style="text-align: left; white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata3->nama_siswa ?></td>
                            <td style="text-align: center;"><?php echo $rowdata3->nama_kelas ?></td>
                            <td style="text-align: center;"><?php echo $rowdata3->tot_all ?></td>
                        </tr>
                    	<?php } 
                      } ?>                       
                </tbody>
            </table>
        </div>
    </div>
</div> 
</div>
</div>
</div>
</div>
<div class="col-lg-12">
    <div class="portlet">
      <div class="portlet-heading bg-info">
        <h3 class="portlet-title">
          PEMETAAN PRODI
      </h3>
      <div class="portlet-widgets">
          <span class="divider"></span>
          <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default2"><i class="ion-minus-round"></i></a>
      </div>
      <div class="clearfix"></div>
  </div>
  <div id="bg-default2" class="panel-collapse collapse in">
    <div class="portlet-body">
      <div class = "row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <h4 class="portlet-title" align="center">
          			PILIHAN PERTAMA
      			</h4>
                <table id="prodi1" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 1%">NISN</th>
                            <th style="text-align: center; width: 15%">Nama Siswa</th>
                            <th style="text-align: center; width: 15%">Perguruan Tinggi</th>
                            <th style="text-align: center; width: 15%">Prodi</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                       $no = 0 ;
                       foreach ($data5 as $rowdata5);
                       if(empty($rowdata5)) { ?>
                       <h4 class="portlet-title" align="center">
          					DATA BELUM DIISI
      				   </h4>
                       <?php }else{
                       foreach ($data4 as $rowdata4) { 
                        if($rowdata4->id_prodi_1 == $rowdata5->id_prodi_1){
                        $no++;              
                        ?> 
                        <tr>
                            <td style="text-align: center;"><?php echo $no ?></td>
                            <td style="text-align: left;"><?php echo $rowdata4->nisn ?></td>
                            <td style="text-align: left; white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata4->nama_siswa ?></td>
                            <td style="text-align: center;"><?php echo $rowdata4->nama_perguruan_tinggi_1 ?></td>
                            <td style="text-align: center;"><?php echo $rowdata4->nama_prodi_1 ?></td>
                        </tr>
                    <?php } } } ?>
                </tbody>
            </table>
        </div>
    </div>
    <div class="col-sm-12">
            <div class="card-box table-responsive">
                <h4 class="portlet-title" align="center">
         			PILIHAN KEDUA
      			</h4>
                <table id="prodi2" class="table table-striped table-bordered dt-responsive nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 1%">NISN</th>
                            <th style="text-align: center; width: 15%">Nama Siswa</th>
                            <th style="text-align: center; width: 15%">Perguruan Tinggi</th>
                            <th style="text-align: center; width: 15%">Prodi</th>
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                       $no = 0 ;
                       if(empty($rowdata5)) { ?>
                       <h4 class="portlet-title" align="center">
          					DATA BELUM DIISI
      				   </h4>
                       <?php }else{
                       foreach ($data4 as $rowdata4) {   
                        if($rowdata4->id_prodi_2 == $rowdata5->id_prodi_2){
                        $no++;              
                        ?> 
                        <tr>
                            <td style="text-align: center;"><?php echo $no ?></td>
                            <td style="text-align: left;"><?php echo $rowdata4->nisn ?></td>
                            <td style="text-align: left; white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata4->nama_siswa ?></td>
                            <td style="text-align: center;"><?php echo $rowdata4->nama_perguruan_tinggi_2 ?></td>
                            <td style="text-align: center;"><?php echo $rowdata4->nama_prodi_2 ?></td>
                        </tr>
                    <?php } } } ?>
                </tbody>
            </table>
        </div>
    </div>
</div> 
</div>
</div>
</div>
</div>
</div>
<!-- END wrapper -->

<script type="text/javascript">
  $(document).ready(function() {
    $('#ranking').DataTable();
    $('#prodi1').DataTable();
    $('#prodi2').DataTable();
} );
</script>
