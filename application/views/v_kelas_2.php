            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">KELAS XI</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
		<!-- Modal Add-->
                <!-- <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <form method="post" action="<?php echo site_url('pengguna/input_guru');?>" enctype='multipart/form-data'>
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">TAMBAH DATA GURU</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Nama Guru</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="text" class="form-control" id="nama_guru" name="nama_guru" placeholder="Masukkan Nama Guru" required>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">NIK</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="number" class="form-control" id="nik" name="nik" placeholder="Masukkan NIK">
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Email</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email">
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Password</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="text" class="form-control" id="password" name="password" placeholder="Masukkan Password">
                           </div>
                         </div>
                       <div class="modal-footer" style="margin-top: 35%">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div> -->
              <!-- END Modal -->
  <div id="bg-default2" class="panel-collapse collapse in">
    <div class="portlet-body">
      <div class = "row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="guru" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 1%">Kategori</th>
                            <th style="text-align: center; width: 15%">Bentuk Pelaggaran</th>
                            <th style="text-align: center; width: 5%">Skor</th>                      
                            <th style="text-align: center; width: 5%">Fitur</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
  					   $no = 0 ;
                       foreach ($data as $rowdata) {   
                        $no++;              
                        ?> 
                        <tr>
                            <td style="text-align: center;"><?php echo $no ?></td>
                            <?php if($rowdata->kategori == 1){ ?>
                            <td style="text-align: center;">Sikap Perilaku</td>
                            <?php } elseif ($rowdata->kategori == 2) { ?>
                            <td style="text-align: center;">Kerajinan</td>
                            <?php }else{ ?>
                            <td style="text-align: center;">Kerapian</td>
                            <?php } ?>
                            <td style="text-align: left; white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->bentuk_pelanggaran ?></td>
                            <td style="text-align: center;"><?php echo $rowdata->skor ?></td>                     
                            <td style="text-align: center;">
                              <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->id_tatib; ?>"><i class="fa fa-edit"></i></button>
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->id_tatib; ?>"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                      	<!-- Modal Update-->
       <!--  <div class="modal fade" id="ModalEdit<?php echo $rowdata->id_pengguna; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
                <form method="post" action="<?php echo site_url('pengguna/update_guru');?>" enctype='multipart/form-data'>
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">UPDATE DATA GURU</h4>
                </div>
                <div class="modal-body">
                  <input type="hidden" id="id_pengguna" name="id_pengguna" value="<?php echo $rowdata->id_pengguna; ?>">
                            <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Nama Guru</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="text" class="form-control" id="nama_guru" name="nama_guru" placeholder="Masukkan Nama Guru" value="<?php echo $rowdata->nama_guru; ?>" required>
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">NIK</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="number" class="form-control" id="nik" name="nik" placeholder="Masukkan NIK" value="<?php echo $rowdata->nik; ?>">
                           </div>
                           <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Email</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="email" class="form-control" id="email" name="email" placeholder="Masukkan Email" value="<?php echo $rowdata->email; ?>">
                           </div>
                         </div>
            <div class="modal-footer" style="margin-top: 25%">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Kirim</button>
            </div>
        </form>
    </div>
</div>
</div> -->
<!-- END Modal -->
                      	<!-- Modal Delete-->
              <!-- <div class="modal fade" id="ModalDelete<?php echo $rowdata->id_pengguna; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <form method="post" action="<?php echo site_url('pengguna/delete_guru');?>">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                          <input type="hidden" id="id_pengguna" name="id_pengguna" value="<?php echo $rowdata->id_pengguna; ?>">
                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->nama_guru."</u>'";?> ?</label>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div> -->
              <!-- END Modal Delete-->
                    	<?php } 
					  ?>                       
                </tbody>
            </table>
        </div>
    </div>
</div> 
</div>
</div>
</div>
<!-- END wrapper -->
<script type="text/javascript">
  $(document).ready(function() {
    $('#guru').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-plus-circle"></i> | Tambah',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });
} );  
</script>