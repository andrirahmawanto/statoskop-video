<!-- ========== Left Sidebar Start ========== -->
<div class="left side-menu" style="padding-top: 8%">
	<div class="sidebar-inner slimscrollleft">
		<!--- Sidemenu -->
		<div id="sidebar-menu">
			<ul>
				<?php if ($this->session->userdata('level')=='1') { ?>
					<li>
						<a href="<?php echo base_url() ?>dashboard" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span></a>
					</li>
                    <li>
                        <a href="<?php echo base_url() ?>dashboard" class="waves-effect"><i class="mdi mdi-chart-bar"></i> <span> Data Pertandingan </span></a>
                    </li>
              		<li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-file-find"></i><span> Explore </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url() ?>kelas/x">Klub</a></li>
                        <li><a href="<?php echo base_url() ?>kelas/xi">Pemain</a></li>
                    </ul>
                    </li>
                    <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="mdi mdi-file-chart"></i><span> Rapor </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="<?php echo base_url() ?>kelas/x">Klub</a></li>
                        <li><a href="<?php echo base_url() ?>kelas/xi">Pemain</a></li>
                    </ul>
                    </li>    
				<?php } elseif ($this->session->userdata('level')=='2') {?>
					<li>
						<a href="<?php echo base_url() ?>dashboard" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Dashboard </span></a>
					</li>
                    <li>
                        <a href="<?php echo base_url() ?>tatib" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Tata Tertib </span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>dashboard/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Prestasi </span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>dashboard/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Pelanggaran </span></a>
                    </li>
				<?php } elseif ($this->session->userdata('level')=='3') {?>
					<li>
                        <a href="<?php echo base_url() ?>tatib" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Tata Tertib </span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>dashboard/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Prestasi </span></a>
                    </li>
                    <li>
                        <a href="<?php echo base_url() ?>dashboard/admin" class="waves-effect"><i class="mdi mdi-view-dashboard"></i> <span> Pelanggaran </span></a>
                    </li>
				
				<?php } ?>
			</ul>
		</div>
		<!-- Sidebar -->
		<div class="clearfix"></div>
	</div>
	<!-- Sidebar -left -->
</div>
<!-- Left Sidebar End