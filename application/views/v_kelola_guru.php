            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">KELOLA DATA GURU</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
                  <!-- Modal Add-->
                <div class="modal fade" id="ModalAdd" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                  <div class="modal-dialog modal-md" role="document">
                    <div class="modal-content">
                      <form method="post" action="<?php echo site_url('kelola/input_kelas');?>" enctype='multipart/form-data'>
                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <h4 class="modal-title" id="myModalLabel">TAMBAH DATA KELAS</h4>
                        </div>
                        <div class="modal-body">
                            <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Nama Kelas</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="text" class="form-control" id="nama_kelas" name="nama_kelas" placeholder="Masukkan Kelas" required>
                           </div>
                         </div>
                       <div class="modal-footer" style="margin-top: 15%">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Simpan</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!-- END Modal -->
  <div id="bg-default2" class="panel-collapse collapse in">
    <div class="portlet-body">
      <div class = "row">
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="kelas" class="table table-striped table-bordered nowrap">
                    <thead>
                        <tr>
                            <th style="text-align: center; width: 1%">No</th>
                            <th style="text-align: center; width: 15%">Nama Guru</th>                   
                            <th style="text-align: center; width: 1%">Fitur</th>
                        </tr>
                    </thead>
                    <tbody>
                      <?php 
  					   $no = 0 ;
                       foreach ($data as $rowdata) {   
                        $no++;              
                        ?> 
                        <tr>
                            <td style="text-align: center;"><?php echo $no ?></td>
                            <td style="text-align: left; white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata->nama_guru ?></td>
                            <td style="text-align: center;">
                              <button type="button" class="btn btn-warning btn-sm" data-toggle="modal" data-target="#ModalEdit<?php echo $rowdata->id_guru; ?>"><i class="fa fa-edit"></i></button>
                              <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#ModalDelete<?php echo $rowdata->id_guru; ?>"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                      	<!-- Modal Update-->
        <div class="modal fade" id="ModalEdit<?php echo $rowdata->id_guru; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog modal-md" role="document">
              <div class="modal-content">
                <form method="post" action="<?php echo site_url('kelola/update_kelas');?>" enctype='multipart/form-data'>
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">UPDATE DATA KELAS</h4>
                </div>
                <div class="modal-body">
                  <input type="hidden" id="id_guru" name="id_guru" value="<?php echo $rowdata->id_guru; ?>">
                            <div class="col-lg-4" style="margin-top: 10px">
                             <label for="txtname">Nama Kelas</label>
                           </div>
                           <div class="col-lg-8" style="margin-bottom: 10px">
                             <input type="text" class="form-control" id="nama_kelas" name="nama_kelas" placeholder="Masukkan Nama Kelas" value="<?php echo $rowdata->nama_kelas; ?>" required>
                           </div>
                  </div>
            <div class="modal-footer" style="margin-top: 25%">
                <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                <button type="submit" id="btnsimpan" name="btnsimpan" class="btn btn-success">Kirim</button>
            </div>
        </form>
    </div>
</div>
</div>
<!-- END Modal -->
                      	<!-- Modal Delete-->
              <div class="modal fade" id="ModalDelete<?php echo $rowdata->id_guru; ?>" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                <div class="modal-dialog" role="document">
                  <div class="modal-content">
                    <form method="post" action="<?php echo site_url('kelola/delete_kelas');?>">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel" style="font-family: sans-serif;">Konfirmasi Hapus Data</h4>
                      </div>
                      <div class="modal-body">
                        <div class="form-group">
                          <input type="hidden" id="id_guru" name="id_guru" value="<?php echo $rowdata->id_guru; ?>">
                          <label style="font-family: sans-serif; font-size: 16px;">Apakah anda yakin, akan menghapus data : <?php echo "'<u>".$rowdata->nama_kelas."</u>'";?> ?</label>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Batal</button>
                        <button type="submit" id="btndelete" name="btndelete" class="btn btn-danger">Hapus</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <!-- END Modal Delete-->
                    	<?php } 
					  ?>                       
                </tbody>
            </table>
        </div>
    </div>
</div> 
</div>
</div>
</div>
<!-- END wrapper -->
<script type="text/javascript">
  $(document).ready(function() {
    $('#kelas').DataTable({
            "language": {
                "lengthMenu": 'Tampilkan _MENU_ Baris',
                "search": '_INPUT_',
                "searchPlaceholder": 'Pencarian . . .'
            },
            dom: 
            "<'row'<'col-md-8'l><'col-md-2 pull-right'f> <'col-md- pull-right' B>>" +
            "<'row'<'col-md-12't>>" +
            "<'row'<'col-md-5'i><'col-md-7'p>>",
            buttons: [
            {
                "text":'<i class="fa fa-plus-circle"></i> | Tambah',"className": 'btn btn-success btn-sm',
                action: function ( e, dt, node, config ) {
                    var selected = dt.row( { selected: true } ).data();
                    $('#ModalAdd').modal('show');
                }
            }
            ]
        });
} );  
</script>