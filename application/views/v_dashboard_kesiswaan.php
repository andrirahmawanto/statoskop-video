            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
              <!-- Start content -->
              <div class="content">
                <div class="container">
                  <div class="row">
                    <div class="col-xs-12">
                      <div class="page-title-box">
                        <h4 class="page-title">DASHBOARD</h4>
                        <ol class="breadcrumb p-0 m-0">
                          <li class="active">
                            Dashboard
                        </li>
                    </ol>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
              <div class="portlet">
                <div class="portlet-heading bg-info">
                  <h3 class="portlet-title">
                    Data PDSS
                </h3>
                <div class="portlet-widgets">
                    <span class="divider"></span>
                    <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default"><i class="ion-minus-round"></i></a>
                </div>
                <div class="clearfix"></div>
            </div>
            <div id="bg-default" class="panel-collapse collapse in">
              <div class="portlet-body">
                <div class = "row">
                  <?php 
                  foreach ($data as $rowdata) {  ?>
                  <!-- end row -->
                  <div class="col-lg-4 col-md-6" align="center">
                    <div class="card-box widget-box-two widget-two-primary">
                      <div class="wigdet-two-content">
                        <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total Siswa</p>
                        <h2><span><?php echo $rowdata->jumlah_siswa ?></span></h2>
                    </div>
                </div>
            </div>
            <!-- end row -->
            <div class="col-lg-4 col-md-6" align="center">
                <div class="card-box widget-box-two widget-two-primary">
                  <div class="wigdet-two-content">
                    <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total IPA Mengisi</p>
                    <h2><span><?php echo $rowdata->jumlah_isi_ipa ?></span></h2>
                </div>
            </div>
        </div>
        <!-- end row -->
        <div class="col-lg-4 col-md-6" align="center">
            <div class="card-box widget-box-two widget-two-primary">
              <div class="wigdet-two-content">
                <p class="m-0 text-uppercase font-600 font-secondary text-overflow" title="Statistics">Total IPS Mengisi</p>
                <h2><span><?php echo $rowdata->jumlah_isi_ips ?></span></h2>
            </div>
        </div>
    </div>
    <!-- end row -->
                  <?php } ?>
</div>
</div>
</div>
</div>
</div>
<div class="col-lg-12">
    <div class="portlet">
      <div class="portlet-heading bg-info">
        <h3 class="portlet-title">
          RANKING PDSS IPA
      </h3>
      <div class="portlet-widgets">
          <span class="divider"></span>
          <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default2"><i class="ion-minus-round"></i></a>
      </div>
      <div class="clearfix"></div>
  </div>
  <div id="bg-default2" class="panel-collapse collapse in">
    <div class="portlet-body">
      <div class = "row">
        <!--DATA TABEL-->
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="ranking_ipa" class="table table-striped nowrap">
                    <thead>
                        <tr>
                            <th rowspan='2' style="text-align: center; width: 1%">Ranking</th>
                            <th rowspan='2' style="text-align: center; width: 1%">NISN</th>
                            <th rowspan='2' style="text-align: center; width: 15%">Nama Siswa</th>
                            <th rowspan='2' style="text-align: center; width: 15%">Kelas</th>
                            <th colspan='17' style="background-color:#e4ede0; text-align: center; width: 15%">Semester 1</th>    
                            <th colspan='17' style="background-color:#efdfbb; text-align: center; width: 15%">Semester 2</th>    
                            <th colspan='17' style="background-color:#ffbdbd; text-align: center; width: 15%">Semester 3</th>    
                            <th colspan='17' style="background-color:#ffffba; text-align: center; width: 15%">Semester 4</th>    
                            <th colspan='17' style="background-color:#baffc9; text-align: center; width: 15%">Semester 5</th>                          
                            <th rowspan='2' style="text-align: center; width: 15%">Total Nilai</th>
                        </tr>
                      	<tr>                            
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#e4ede0; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#e4ede0; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Kewirausahaan</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Fisika</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Kimia</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Biologi</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Matematika Minat</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Bahasa Jepang</th>                           
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Sosiologi</th>  
                          
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#efdfbb; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#efdfbb; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Kewirausahaan</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Fisika</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Kimia</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Biologi</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Matematika Minat</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Bahasa Jepang</th>                           
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Sosiologi</th> 
                          
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#ffbdbd; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#ffbdbd; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Kewirausahaan</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Fisika</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Kimia</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Biologi</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Matematika Minat</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Bahasa Jepang</th>                           
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Sosiologi</th>  
                          
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#ffffba; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#ffffba; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Kewirausahaan</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Fisika</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Kimia</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Biologi</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Matematika Minat</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Bahasa Jepang</th>                           
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Sosiologi</th> 
                          
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#baffc9; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#baffc9; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Kewirausahaan</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Fisika</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Kimia</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Biologi</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Matematika Minat</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Bahasa Jepang</th>                           
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Sosiologi</th>  
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                       $no = 0 ;
                       foreach ($data2 as $rowdata2) {   
                       $no++;              
                       ?> 
                       <tr>
                        <td style="text-align: center;"><?php echo $no ?></td>
                        <td style="text-align: left;"><?php echo $rowdata2->nisn ?></td>
                        <td style="text-align: left; white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata2->nama_siswa ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nama_kelas ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_agm1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwn1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_ind1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_mat1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sejin1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_big1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_snb1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_pjo1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwr1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_fis1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kim1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_bio1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_matmin1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_eko1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sig1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_jep1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sos1 ?></td>                         
                        
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_agm2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwn2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_ind2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_mat2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sejin2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_big2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_snb2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_pjo2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwr2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_fis2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kim2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_bio2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_matmin2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_eko2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sig2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_jep2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sos2 ?></td>
                         
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_agm3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwn3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_ind3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_mat3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sejin3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_big3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_snb3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_pjo3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwr3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_fis3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kim3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_bio3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_matmin3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_eko3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sig3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_jep3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sos3 ?></td>
                         
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_agm4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwn4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_ind4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_mat4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sejin4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_big4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_snb4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_pjo4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwr4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_fis4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kim4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_bio4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_matmin4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_eko4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sig4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_jep4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sos4 ?></td>
                         
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_agm5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwn5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_ind5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_mat5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sejin5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_big5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_snb5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_pjo5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kwr5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_fis5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_kim5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_bio5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_matmin5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_eko5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sig5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_jep5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata2->nilai_sos5 ?></td>
                         
                        <td style="text-align: center;"><?php echo $rowdata2->tot_all ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div> <!-- demo-box -->
</div> <!-- content -->
</div>
</div>
</div>
<div class="col-lg-12">
    <div class="portlet">
      <div class="portlet-heading bg-info">
        <h3 class="portlet-title">
          RANKING PDSS IPS
      </h3>
      <div class="portlet-widgets">
          <span class="divider"></span>
          <a data-toggle="collapse" data-parent="#accordion1" href="#bg-default2"><i class="ion-minus-round"></i></a>
      </div>
      <div class="clearfix"></div>
  </div>
  <div id="bg-default2" class="panel-collapse collapse in">
    <div class="portlet-body">
      <div class = "row">
        <!--DATA TABEL-->
        <div class="col-sm-12">
            <div class="card-box table-responsive">
                <table id="ranking_ips" class="table table-striped nowrap">
                    <thead>
                        <tr>
                            <th rowspan='2' style="text-align: center; width: 1%">Ranking</th>
                            <th rowspan='2' style="text-align: center; width: 1%">NISN</th>
                            <th rowspan='2' style="text-align: center; width: 15%">Nama Siswa</th>
                            <th rowspan='2' style="text-align: center; width: 15%">Kelas</th>
                            <th colspan='15' style="background-color:#e4ede0; text-align: center; width: 15%">Semester 1</th>    
                            <th colspan='15' style="background-color:#efdfbb; text-align: center; width: 15%">Semester 2</th>    
                            <th colspan='15' style="background-color:#ffbdbd; text-align: center; width: 15%">Semester 3</th>    
                            <th colspan='15' style="background-color:#ffffba; text-align: center; width: 15%">Semester 4</th>    
                            <th colspan='15' style="background-color:#baffc9; text-align: center; width: 15%">Semester 5</th>                          
                            <th rowspan='2' style="text-align: center; width: 15%">Total Nilai</th>
                        </tr>
                      	<tr>                            
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#e4ede0; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#e4ede0; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Kewirausahaan</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Sejarah (Peminatan)</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Geografi</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Sosiologi</th>                              
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                      
                            <th style="background-color:#e4ede0; text-align: center; width: 1%">Biologi</th>  
                          
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#efdfbb; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#efdfbb; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Kewirausahaan</th>                          
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Sejarah (Peminatan)</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Geografi</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Sosiologi</th>                              
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                      
                            <th style="background-color:#efdfbb; text-align: center; width: 1%">Biologi</th> 
                          
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#ffbdbd; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#ffbdbd; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Kewirausahaan</th>                          
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Sejarah (Peminatan)</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Geografi</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Sosiologi</th>                              
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                      
                            <th style="background-color:#ffbdbd; text-align: center; width: 1%">Biologi</th> 
                          
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#ffffba; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#ffffba; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Kewirausahaan</th>                           
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Sejarah (Peminatan)</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Geografi</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Sosiologi</th>                              
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                      
                            <th style="background-color:#ffffba; text-align: center; width: 1%">Biologi</th> 
                          
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Pendidikan Agama</th> 
                          	<th style="background-color:#baffc9; text-align: center; width: 1%">Pendidikan Kewarganegaraan</th>                            
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Bahasa Indonesia</th> 
                          	<th style="background-color:#baffc9; text-align: center; width: 1%">Matematika</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Sejarah</th>                            
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Bahasa Inggris</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Seni Budaya</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">PJOK</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Kewirausahaan</th>                          
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Sejarah (Peminatan)</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Geografi</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Ekonomi</th>                             
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Sosiologi</th>                              
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Bahasa dan Sastra Inggris</th>                      
                            <th style="background-color:#baffc9; text-align: center; width: 1%">Biologi</th> 
                        </tr>
                    </thead>
                    <tbody>
                       <?php
                       $no = 0 ;
                       foreach ($data3 as $rowdata3) {   
                       $no++;              
                       ?> 
                       <tr>
                        <td style="text-align: center;"><?php echo $no ?></td>
                        <td style="text-align: left;"><?php echo $rowdata3->nisn ?></td>
                        <td style="text-align: left; white-space: pre-wrap; word-wrap: break-word;"><?php echo $rowdata3->nama_siswa ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nama_kelas ?></td>
                         
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_agm1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwn1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_ind1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_mat1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sejin1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_big1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_snb1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_pjo1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwr1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sej1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_geo1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_eko1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sos1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sig1 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_bio1 ?></td>  
                         
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_agm2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwn2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_ind2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_mat2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sejin2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_big2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_snb2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_pjo2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwr2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sej2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_geo2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_eko2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sos2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sig2 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_bio2 ?></td>  
                         
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_agm3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwn3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_ind3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_mat3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sejin3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_big3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_snb3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_pjo3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwr3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sej3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_geo3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_eko3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sos3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sig3 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_bio3 ?></td>  
                         
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_agm4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwn4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_ind4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_mat4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sejin4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_big4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_snb4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_pjo4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwr4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sej4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_geo4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_eko4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sos4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sig4 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_bio4 ?></td>  
                         
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_agm5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwn5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_ind5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_mat5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sejin5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_big5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_snb5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_pjo5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_kwr5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sej5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_geo5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_eko5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sos5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_sig5 ?></td>
                        <td style="text-align: center;"><?php echo $rowdata3->nilai_bio5 ?></td>  
                                                  
                        <td style="text-align: center;"><?php echo $rowdata3->tot_all ?></td>
                    </tr>
                    <?php } ?>
                </tbody>
            </table>
        </div>
    </div>
</div> <!-- demo-box -->
</div> <!-- content -->
</div>
</div>
</div>
</div>
<!-- END wrapper -->

<script type="text/javascript">
  $(document).ready(function() {
    $('#ranking_ipa').DataTable(
      {
       "language": {
        "lengthMenu": 'Tampilkan _MENU_ Baris',
        "search": '_INPUT_',
        "searchPlaceholder": 'Pencarian . . .'
      },
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        fixedColumns:   {
            leftColumns: 4,
            rightColumns: 1
        },
      dom: 
      "<'row'<'col-md-9'l><'col-md-1 right' B><'col-md-2' f>> " +
      "<'row'<'col-md-12't>>" +
      "<'row'<'col-md-5'i><'col-md-7'p>>",
      buttons: [
      {
                extend:    'excelHtml5',
                text:      '<i class="mdi mdi-file"></i> Excel',
                className: 'btn btn-success btn-sm',
                title: 'Ranking PDSS IPA'
            }
      ]
    });
    $('#ranking_ips').DataTable(
      {
       "language": {
        "lengthMenu": 'Tampilkan _MENU_ Baris',
        "search": '_INPUT_',
        "searchPlaceholder": 'Pencarian . . .'
      },
        scrollY:        "300px",
        scrollX:        true,
        scrollCollapse: true,
        fixedColumns:   {
            leftColumns: 4,
            rightColumns: 1
        },
      dom: 
      "<'row'<'col-md-9'l><'col-md-1 right' B><'col-md-2' f>> " +
      "<'row'<'col-md-12't>>" +
      "<'row'<'col-md-5'i><'col-md-7'p>>",
      buttons: [
      {
                extend:    'excelHtml5',
                text:      '<i class="mdi mdi-file"></i> Excel',
                className: 'btn btn-success btn-sm',
                title: 'Ranking PDSS IPS'
            }
      ]
    });
} );
</script>

